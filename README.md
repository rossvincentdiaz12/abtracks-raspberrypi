# ABTRACKS Raspberry pi

Embedded program to raspberry pi to send the location to the ABTR

## Getting Started
Wiring
Since we are using a Raspberry Pi with this GPS module, we’ll need to have access to the Raspberry Pi’s UART (Universal Asynchronous Receiver Transmitter) interface.

The details of why we’re using this protocol extend far beyond the scope of this project. In essence, this is the best way to send and receive data serially from our module to the RPI. The TX (transmitter) pin of the GPS module sends bits to the RX (receiver) pin of the RPI and vice versa.

rpi pin 1 to possitive
rpi pin 6 to GND
rpi pin 8 to tx
rpi pin 10 to rx


### Prerequisites

PUTTY - to remote you raspberry pi to your computer
Enabling UART
Configure I2C
Configure SPI

### Installing

Setting up 3G/4G LTE (Optional)

If you want to be able to have your Raspberry Pi have GPS capabilities without WiFi dependance, then you’ll need to use an LTE shield. The setup for this is fairly simple and you’ll need to go to [this page](https://sixfab.com/category/raspberry-pi-3g-4glte-base-shield/) for the setup.

Once you’re on the page, carefully follow the “Hardware Setup” as well as the “Software Setup” instruction sections. Here you will essentially download the necessary libraries, packages, and GitHub repositories to run SIXFAB’s software on the hardware.

Lastly, follow the “Autoconnect on Reboot” section to make sure you’re RPI connects to LTE on startup.


Enabling UART on the Raspberry Pi
# SSH into your RPI and type in:
* sudo raspi-config

You should then get a graphical interface in your terminal.

select Interfacing Option
then P6
select no
then yes

# then
* sudo reboot


Configure I2C on the RPI


This step is required to use the CircuitPython Library in Step 4 as many of the I2C drivers are used in that library. This is essentially a 2-wire bus protocol that allows one chip to talk with another. Please reference [this page](https://sixfab.com/category/raspberry-pi-3g-4glte-base-shield/) if you’re interested in learning more about bus protocols.

# First, ssh into your RPI. Next, install the I2C-tools utility:
* sudo apt-get install -y python-smbus
* sudo apt-get install -y i2c-tools

# Next, we need to install Kernal Support. Like before, in your terminal type in
* sudo raspi-config

select Interfacing Option
then A7
select yes
then yes

# then
* sudo reboot

Configure SPI on the RPI

SPI (Serial Peripheral Interface) is another bus protocol that synchronizes serial communication between chips. Again, for those interested in learning more, check out [this Wiki page](https://en.wikipedia.org/wiki/Serial_Peripheral_Interface).

# Just like before, ssh into your RPI and do a:
* sudo raspi-config

select Interfacing Option
then P4 SPI
select yes

# then
* sudo reboot

## Deployment

open you terminal and run the code

sudo python3 GPS_API.py

## Built With

* [Python3](https://www.python.org/download/releases/3.0/) 
* [Rasbberry Pi Model 3B](https://www.raspberrypi.org/blog/raspberry-pi-3-model-bplus-sale-now-35/) 
* [UBLOX NEO 6M GPS Module](https://www.elecdesignworks.com/shop/ublox-neo-6m-gps-module-red-mini-indoor-antenna/)

## Contributing

Please read [CONTRIBUTING.md](https://gist.github.com/PurpleBooth/b24679402957c63ec426) for details on our code of conduct, and the process for submitting pull requests to us.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details